PHP Test
========

You need to have PHP-cli installed on your computer.

1. Install the composer dependencies: php composer.phar install

2. Run ./phpunit . The tests will fail.

3. Make the tests pass without changing the assertions.

Good luck! :)
