<?php

namespace Warpspeed;

use Warpspeed\Services\AbstractClass;
use Warpspeed\Services\Client;

class WarpspeedClient extends AbstractClass {

    var $client;

    /**
     * WarpspeedClient constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    //test this function
    function doSomething()
    {
        return $this->client->execute();
    }

}
