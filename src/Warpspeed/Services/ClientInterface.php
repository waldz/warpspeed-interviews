<?php

namespace Warpspeed\Services;

interface ClientInterface
{
    public function execute();
}