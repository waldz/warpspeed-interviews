<?php

namespace Warpspeed\Services;

class Client implements ClientInterface
{
    protected $message;

    /**
     * @param string $message
     */
    public function __construct($message = '')
    {
        $this->message = $message;
    }

    public function execute()
    {
        return 'works ' . $this->message;
    }
}