<?php
namespace Warpspeed\Tests;

use \PHPUnit_Framework_TestCase;
use Warpspeed\Services\Client;
use Warpspeed\WarpspeedClient;

class TestWarpspeedClient extends \PHPUnit_Framework_TestCase
{

    public function testExecute()
    {
        $connectionMock = new Client('great');
        $warspeedClient = new WarpspeedClient($connectionMock);

        //do not change this assert
        $this->assertEquals($warspeedClient->doSomething(),'works great');
    }

    public function testExecuteAgain()
    {
        $connectionMock = new Client('fine');
        $warspeedClient = new WarpspeedClient($connectionMock);

        //do not change this assert
        $this->assertEquals($warspeedClient->doSomething(),'works fine');
    }
}
